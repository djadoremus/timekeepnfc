package ph.adoremus.timekeepnfc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ph.adoremus.timekeepnfc.db.DbProject;
import ph.adoremus.timekeepnfc.model.Project;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.app.LauncherActivity.ListItem;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class TaskEncodingActivity extends Activity implements OnClickListener, OnItemSelectedListener {

	private Spinner spnProjectName;
	private TextView tvTagId;
	private Spinner spnCategories;
	private EditText etTaskDescription;
	private Button btnTaskTrigger;
	private ListView lsPendingTasks;

	private NfcAdapter nfcAdapter;
	private Parcelable[] rawMsgs;
	
	private String[] categories = {"Planning", "Meeting", "Development"};
	private String[] projectNames;
	
	private DbProject dbProject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_encoding);
		tvTagId = (TextView) findViewById(R.id.tvTagID);
		spnProjectName = (Spinner) findViewById(R.id.spnProjecName);
		spnCategories = (Spinner) findViewById(R.id.spnCategories);
		etTaskDescription = (EditText) findViewById(R.id.etTaskDescription);
		btnTaskTrigger = (Button) findViewById(R.id.btnTaskTrigger);
		lsPendingTasks = (ListView) findViewById(R.id.lsPendingTasks);

		btnTaskTrigger.setOnClickListener(this);

		nfcAdapter = NfcAdapter.getDefaultAdapter(this);
		loadTagData();
		
		ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
		spnCategories.setAdapter(categoryAdapter);
		
		etTaskDescription.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				btnTaskTrigger.setText(R.string.static_startTask);
			}
		});
		
		dbProject = new DbProject(this);
		dbProject.open();
		
		String[] projectNames = dbProject.findAllProjectListEntries();
		Log.d("TIMEKEEPNFC", "size : " + projectNames.length);
		for (String pn : projectNames){
			Log.d("TIMEKEEPNFC", "name : " + pn);
		}
		ArrayAdapter<String> projectNamesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, projectNames);
		spnProjectName.setAdapter(projectNamesAdapter);
		spnProjectName.setOnItemSelectedListener(this);
		
		
		final List<Project> data = dbProject.getPendingData();
		List<String> pendingItems = new ArrayList<String>();
		for (int i=0; i<data.size(); i++){
			Project project = data.get(i);
			pendingItems.add(project.getTask());
		}
		ArrayAdapter<String> pendingItemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pendingItems);
		lsPendingTasks.setAdapter(pendingItemsAdapter);
		lsPendingTasks.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Project project = data.get(arg2);
				btnTaskTrigger.setTag(project.getId());
				for (int i=0; i<categories.length; i++){
					String category = categories[i];
					if (category.equals(project.getCategory())){
						spnCategories.setSelection(i);
						break;
					}
				}
				etTaskDescription.setText(project.getTask());
				btnTaskTrigger.setText(R.string.static_finishTask);
				
			}
		});		
		dbProject.close();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_encoding, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		dbProject.open();
		if (v == btnTaskTrigger) {
			if (getResources().getString(R.string.static_startTask).equals(
					((Button) v).getText())) {
				btnTaskTrigger.setText(R.string.static_finishTask);
				
				Project project = new Project();
				project.setName(spnProjectName.getSelectedItem().toString());
				project.setCategory(spnCategories.getSelectedItem().toString());
				project.setTask(etTaskDescription.getText().toString());
				project.setStarted(new Date());
				
				long projectId = dbProject.addEntry(project);
				btnTaskTrigger.setTag(Integer.parseInt("" + projectId));
				Log.i(this.toString(), "id = " + projectId);
			} else {
				//assumes you always selects an item from the list
				btnTaskTrigger.setText(R.string.static_startTask);
				Project project = dbProject.getEntry((Integer) btnTaskTrigger.getTag());
				project.setFinished(new Date());
				dbProject.updateEntry(project);
			}
		}
		final List<Project> data = dbProject.getPendingData();
		List<String> pendingItems = new ArrayList<String>();
		for (int i=0; i<data.size(); i++){
			Project project = data.get(i);
			pendingItems.add(project.getTask());
		}
		ArrayAdapter<String> pendingItemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pendingItems);
		lsPendingTasks.setAdapter(pendingItemsAdapter);
		lsPendingTasks.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Project project = data.get(arg2);
				btnTaskTrigger.setTag(project.getId());
				for (int i=0; i<categories.length; i++){
					String category = categories[i];
					if (category.equals(project.getCategory())){
						spnCategories.setSelection(i);
						break;
					}
				}
				etTaskDescription.setText(project.getTask());
				btnTaskTrigger.setText(R.string.static_finishTask);
				
			}
		});		
		dbProject.close();
	}

	private void loadTagData() {
		Intent intentNFC = getIntent();
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intentNFC.getAction())) {
			Tag tag = intentNFC.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			tvTagId.setText(new String(bytesToHexString(tag.getId())));
			rawMsgs = intentNFC
					.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			if (rawMsgs != null) {
				for (int i = 0; i < rawMsgs.length; i++) {
					NdefMessage nMsg = (NdefMessage) rawMsgs[i];

					for (int j = 0; j < nMsg.getRecords().length; j++) {
						NdefRecord rec = nMsg.getRecords()[j];

						String tagProjectName = new String(rec.getPayload());
						for (int k=0; k<projectNames.length; k++){
							String projectName = projectNames[k];
							if (projectName == tagProjectName){
								spnProjectName.setSelection(k);
								break;
							}
						}
					}
				}
			}
		}
	}

	private String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("0x");
		if (src == null || src.length <= 0) {
			return null;
		}

		char[] buffer = new char[2];
		for (int i = 0; i < src.length; i++) {
			buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
			buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
			System.out.println(buffer);
			stringBuilder.append(buffer);
		}

		return stringBuilder.toString();
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		//filter listings to those related to the selected project
		Log.d("TIMEKEEPNFC", "item selected " );
		String name = (String) spnProjectName.getAdapter().getItem(arg2);
		dbProject.open();
		lsPendingTasks.invalidate();
		ArrayAdapter<String> pendingItemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dbProject.getEntries(name));
		lsPendingTasks.setAdapter(pendingItemsAdapter);
		((ArrayAdapter<String>)lsPendingTasks.getAdapter()).notifyDataSetChanged();
		dbProject.close();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		//remove all filters and display all pending tasks
		Log.d("TIMEKEEPNFC", "item deselected");
	}
}
