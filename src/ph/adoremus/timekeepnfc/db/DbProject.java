package ph.adoremus.timekeepnfc.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ph.adoremus.timekeepnfc.model.Project;
import ph.adoremus.timekeepnfc.model.ProjectList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbProject {
	private Context context;
	private ProjectDBHelper dbHelper;
	private SQLiteDatabase sqLiteDatabase;

	private static final String PROJECT_TABLE_NAME = "PROJECT";
	private static final String PROJECTLIST_TABLE_NAME = "PROJECT_LIST";
	private static final String DB_NAME = "TIMEKEEP_NFC";
	private static final Integer DB_VERSION = 1;

	public DbProject(Context context) {
		this.context = context;
	}

	public DbProject open() {
		dbHelper = new ProjectDBHelper(context);
		sqLiteDatabase = dbHelper.getWritableDatabase();
		return this;
	}

	public long addEntry(Project project) {
		ContentValues cvProject = new ContentValues();
		cvProject.put("NAME", project.getName());
		cvProject.put("CATEGORY", project.getCategory());
		cvProject.put("TASK", project.getTask());
		cvProject.put("DATE_STARTED", project.getStarted().toString());
		cvProject.put("DATE_ENDED", project.getFinished() != null ? project
				.getFinished().toString() : null);

		return sqLiteDatabase.insert(PROJECT_TABLE_NAME, null, cvProject);
	}

	public Project getEntry(Integer id) {
		Cursor cursor = sqLiteDatabase.rawQuery(
				"Select * from Project where id = ?",
				new String[] { id.toString() });
		String[] columns = new String[] { "ID", "NAME", "CATEGORY", "TASK",
				"DATE_STARTED", "DATE_ENDED" };
		Integer[] cIndex = new Integer[] { cursor.getColumnIndex(columns[0]),
				cursor.getColumnIndex(columns[1]),
				cursor.getColumnIndex(columns[2]),
				cursor.getColumnIndex(columns[3]),
				cursor.getColumnIndex(columns[4]),
				cursor.getColumnIndex(columns[5]) };
		Project project = new Project();
		while (cursor.moveToNext()) {
			project.setId(cursor.getInt(cIndex[0]));
			project.setName(cursor.getString(cIndex[1]));
			project.setCategory(cursor.getString(cIndex[2]));
			project.setTask(cursor.getString(cIndex[3]));
			project.setStarted(new Date(cursor.getString(cIndex[4])));
			project.setFinished(cursor.getString(cIndex[5]) != null ? new Date(
					cursor.getString(cIndex[5])) : null);
		}
		return project;
	}

	public String[] getEntries(String name) {
		Cursor cursor = sqLiteDatabase.rawQuery(
				"Select * from Project where name = ?",
				new String[] { name });
		String[] columns = new String[] { "ID", "NAME", "CATEGORY", "TASK",
				"DATE_STARTED", "DATE_ENDED" };
		Integer[] cIndex = new Integer[] { cursor.getColumnIndex(columns[0]),
				cursor.getColumnIndex(columns[1]),
				cursor.getColumnIndex(columns[2]),
				cursor.getColumnIndex(columns[3]),
				cursor.getColumnIndex(columns[4]),
				cursor.getColumnIndex(columns[5]) };
		String[] projects = new String[cursor.getCount()];
		int i=0;
		while (cursor.moveToNext()) {
			Project project = new Project();
			project.setId(cursor.getInt(cIndex[0]));
			project.setName(cursor.getString(cIndex[1]));
			project.setCategory(cursor.getString(cIndex[2]));
			project.setTask(cursor.getString(cIndex[3]));
			project.setStarted(new Date(cursor.getString(cIndex[4])));
			project.setFinished(cursor.getString(cIndex[5]) != null ? new Date(
					cursor.getString(cIndex[5])) : null);
			
			projects[i] = project.getTask();
			i++;
		}
		return projects;
	}

	public long updateEntry(Project project) {
		ContentValues cvProject = new ContentValues();
		cvProject.put("NAME", project.getName());
		cvProject.put("CATEGORY", project.getCategory());
		cvProject.put("TASK", project.getTask());
		cvProject.put("DATE_STARTED", project.getStarted().toString());
		cvProject.put("DATE_ENDED", project.getFinished() != null ? project
				.getFinished().toString() : null);

		return sqLiteDatabase.update(PROJECT_TABLE_NAME, cvProject, "ID = ?",
				new String[] { project.getId().toString() });
	}

	public List<Project> getPendingData() {
		String[] columns = new String[] { "ID", "NAME", "CATEGORY", "TASK",
				"DATE_STARTED", "DATE_ENDED" };
		Cursor cursor = sqLiteDatabase.rawQuery(
				"select * from Project where date_ended is null", null);

		Integer[] cIndex = new Integer[] { cursor.getColumnIndex(columns[0]),
				cursor.getColumnIndex(columns[1]),
				cursor.getColumnIndex(columns[2]),
				cursor.getColumnIndex(columns[3]),
				cursor.getColumnIndex(columns[4]),
				cursor.getColumnIndex(columns[5]) };

		List<Project> data = new ArrayList<Project>();
		while (cursor.moveToNext()) {
			if (cursor.getString(cIndex[5]) == null) {
				Project project = new Project();
				project.setId(cursor.getInt(cIndex[0]));
				project.setName(cursor.getString(cIndex[1]));
				project.setCategory(cursor.getString(cIndex[2]));
				project.setTask(cursor.getString(cIndex[3]));
				project.setStarted(new Date(cursor.getString(cIndex[4])));
				project.setFinished(cursor.getString(cIndex[5]) != null ? new Date(
						cursor.getString(cIndex[5])) : null);

				data.add(project);
			}
		}

		return data;
	}

	public List<Project> getFinishedData() {
		String[] columns = new String[] { "ID", "NAME", "CATEGORY", "TASK",
				"DATE_STARTED", "DATE_ENDED" };
		Cursor cursor = sqLiteDatabase
				.rawQuery(
						"select * from Project where date_ended is not null order by date_started asc",
						null);

		Integer[] cIndex = new Integer[] { cursor.getColumnIndex(columns[0]),
				cursor.getColumnIndex(columns[1]),
				cursor.getColumnIndex(columns[2]),
				cursor.getColumnIndex(columns[3]),
				cursor.getColumnIndex(columns[4]),
				cursor.getColumnIndex(columns[5]) };

		List<Project> data = new ArrayList<Project>();
		while (cursor.moveToNext()) {
			Project project = new Project();
			project.setId(cursor.getInt(cIndex[0]));
			project.setName(cursor.getString(cIndex[1]));
			project.setCategory(cursor.getString(cIndex[2]));
			project.setTask(cursor.getString(cIndex[3]));
			project.setStarted(new Date(cursor.getString(cIndex[4])));
			project.setFinished(cursor.getString(cIndex[5]) != null ? new Date(
					cursor.getString(cIndex[5])) : null);

			data.add(project);
		}

		return data;
	}
	
	public Long addListEntry(ProjectList pl){
		ContentValues cvProject = new ContentValues();
		cvProject.put("NAME", pl.getName());

		return sqLiteDatabase.insert(PROJECTLIST_TABLE_NAME, null, cvProject);
	}
	
	public String[] findAllProjectListEntries(){
		String[] columns = new String[] { "ID", "NAME" };
		Cursor cursor = sqLiteDatabase.rawQuery(
				"select * from Project_list", null);

		Integer[] cIndex = new Integer[] { cursor.getColumnIndex(columns[0]),
				cursor.getColumnIndex(columns[1]) };

		List<ProjectList> data = new ArrayList<ProjectList>();
		while (cursor.moveToNext()) {
			ProjectList pl = new ProjectList();
			
			pl.setId(cursor.getInt(cIndex[0]));
			pl.setName(cursor.getString(cIndex[1]));
			
			data.add(pl);
		}
		
		String[] arrData = new String[data.size()];
		for (int i=0; i<data.size(); i++){
			ProjectList pl = data.get(i);
			arrData[i] = pl.getName();
		}
		
		return arrData;
	}
	

	public void close() {
		dbHelper.close();
	}

	private static class ProjectDBHelper extends SQLiteOpenHelper {

		public ProjectDBHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			StringBuilder sbCreateProjectTbl = new StringBuilder();
			sbCreateProjectTbl
					.append("CREATE TABLE IF NOT EXISTS ").append(PROJECT_TABLE_NAME)
					.append(" (ID INTEGER PRIMARY KEY AUTOINCREMENT, ")
					.append("NAME TEXT, ").append("CATEGORY TEXT, ")
					.append("TASK TEXT, ").append("DATE_STARTED TEXT, ")
					.append("DATE_ENDED TEXT").append(");");
			db.execSQL(sbCreateProjectTbl.toString());

			StringBuilder sbCreateProjectListTbl = new StringBuilder();
			sbCreateProjectListTbl
					.append("CREATE TABLE IF NOT EXISTS ").append(PROJECTLIST_TABLE_NAME)
					.append(" (ID INTEGER PRIMARY KEY AUTOINCREMENT, ")
					.append("NAME TEXT);");
			db.execSQL(sbCreateProjectListTbl.toString());
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + PROJECT_TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + PROJECTLIST_TABLE_NAME);
			onCreate(db);
		}

	}

}
