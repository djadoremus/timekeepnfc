package ph.adoremus.timekeepnfc.model;

import java.util.Date;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Project {
	private Integer id;
	private String name;
	private String category;
	private String task;
	private Date started;
	private Date finished;
	
	public  Integer getId() {
		return id;
	}

	public  void setId(Integer id) {
		this.id = id;
	}

	public  String getName() {
		return name;
	}

	public  void setName(String name) {
		this.name = name;
	}

	public  String getCategory() {
		return category;
	}

	public  void setCategory(String category) {
		this.category = category;
	}

	public  String getTask() {
		return task;
	}

	public  void setTask(String task) {
		this.task = task;
	}

	public  Date getStarted() {
		return started;
	}

	public  void setStarted(Date started) {
		this.started = started;
	}

	public  Date getFinished() {
		return finished;
	}

	public  void setFinished(Date finished) {
		this.finished = finished;
	}
	
	@Override
	public String toString() {
		return "{id:" + this.id + ", name:" + this.name + ", category:" + this.category + ", task:" + this.task + ", started:" + this.started + ", finished:" + this.finished + "}";
	}

}
