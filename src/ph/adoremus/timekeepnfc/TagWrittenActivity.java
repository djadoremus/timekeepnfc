package ph.adoremus.timekeepnfc;

import ph.adoremus.timekeepnfc.db.DbProject;
import android.app.Activity;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class TagWrittenActivity extends Activity {

	private NfcAdapter nfcAdapter;
	private TextView tvWrittenTag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tag_written);
		nfcAdapter = NfcAdapter.getDefaultAdapter(this);
		
		String projectName = getIntent().getStringExtra("nfcProjectName");
		tvWrittenTag = (TextView) findViewById(R.id.static_writtentag);
		tvWrittenTag.setText("Project " + projectName + " has been written on the Tag!");
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tag_written, menu);
		return true;
	}

}
