package ph.adoremus.timekeepnfc;

import java.util.ArrayList;
import java.util.List;

import ph.adoremus.timekeepnfc.db.DbProject;
import ph.adoremus.timekeepnfc.model.Project;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class TaskReportActivity extends Activity {
	private ListView lsAllTasks;
	private DbProject dbProject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_report);
		lsAllTasks = (ListView) findViewById(R.id.lsAllTasks);
		dbProject = new DbProject(this);
		dbProject.open();
		
		final List<Project> data = dbProject.getFinishedData();
		List<String> finishedItems = new ArrayList<String>();
		for (int i=0; i<data.size(); i++){
			Project project = data.get(i);
			finishedItems.add(project.getCategory() + " " + project.getTask() + " " + project.getStarted() + " " + project.getFinished());
		}
		ArrayAdapter<String> pendingItemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, finishedItems);
		lsAllTasks.setAdapter(pendingItemsAdapter);
		dbProject.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_report, menu);
		return true;
	}

}
