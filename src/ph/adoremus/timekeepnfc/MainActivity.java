package ph.adoremus.timekeepnfc;

import java.io.IOException;
import java.nio.charset.Charset;

import ph.adoremus.timekeepnfc.db.DbProject;
import ph.adoremus.timekeepnfc.model.ProjectList;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private EditText etProjectName;
	private Button btnWriteToTag;
	private Button btnAddToDB;
	private NfcAdapter nfcAdapter;
	
	private DbProject dbProject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		nfcAdapter = NfcAdapter.getDefaultAdapter(this);
		btnWriteToTag = (Button) findViewById(R.id.btnWriteToTag);
		btnAddToDB = (Button) findViewById(R.id.btnAddToDB);
		etProjectName = (EditText) findViewById(R.id.etProject);

		btnWriteToTag.setOnClickListener(this);
		btnAddToDB.setOnClickListener(this);
		
		dbProject = new DbProject(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		
		if (getResources().getString(R.string.title_activity_task_encoding).equals(item.getTitle())){
			Intent iEncode = new Intent(this, TaskEncodingActivity.class);
			startActivity(iEncode);
		} else if (getResources().getString(R.string.title_activity_task_report).equals(item.getTitle())){
			Intent iReport = new Intent(this, TaskReportActivity.class);
			startActivity(iReport);
		}
		
		return true;
	}
	
	@Override
	public void onClick(View v) {
		String projectName = etProjectName.getText().toString();
		if (v == btnWriteToTag) {
			NdefRecord record = NdefRecord.createMime(
					"application/ph.adoremus.timekeepnfc",
					projectName.getBytes(Charset.forName("US-ASCII")));
			NdefMessage msg = new NdefMessage(record);

			Intent intentWriteToTag = new Intent(this, getClass());
			intentWriteToTag.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			intentWriteToTag.putExtra("nfcProject", msg);
			PendingIntent piWriteToTag = PendingIntent.getActivity(this, 0,
					intentWriteToTag, 0);
			nfcAdapter.enableForegroundDispatch(this, piWriteToTag,
					new IntentFilter[] { new IntentFilter(
							NfcAdapter.ACTION_TAG_DISCOVERED) }, null);
			Toast.makeText(this, "Please tap your device on a NFC Tag to write the information", Toast.LENGTH_LONG).show();
		} else if (v == btnAddToDB){
			Log.d("TIMEKEEPNFC", "in add to db block");
			ProjectList pl = new ProjectList();
			pl.setName(projectName);
			
			dbProject.open();
			
			dbProject.addListEntry(pl);
			
			dbProject.close();
			Toast.makeText(this, "Project name " + (pl != null ? pl.getName() : "yoyoyo") + " has been added to the listing.", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		try {
			if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
				Tag discoveredTag = intent
						.getParcelableExtra(NfcAdapter.EXTRA_TAG);
				NdefMessage msg = (NdefMessage) intent
						.getParcelableExtra("nfcProject");
				Ndef ndef = Ndef.get(discoveredTag);
				ndef.connect();
				ndef.writeNdefMessage(msg);
				ndef.close();
				
				Intent intentWriteToTag = new Intent(this, TagWrittenActivity.class);
				intentWriteToTag.putExtra("nfcProjectName", etProjectName.getText().toString());
				startActivity(intentWriteToTag);
			}
		} catch (IOException e) {
			Log.e(this.toString(), e.getMessage());
		} catch (FormatException e) {
			Log.e(this.toString(), e.getMessage());
		}
	}

}
